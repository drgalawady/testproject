// exersise 3
// grep -v   // tail -n 1   // tail -n 1 Datei.fasta > nursequenz   // wc -m 
// skript für den nextflow processer zur aufgabe 3
// Eingabe: FASTA-Datei 
// Ausgabe: Datei mit Längen der Sequenzen

nextflow.enable.dsl=2

process split_file {                      // process # 1

    // publishDir "/tmp/seqLang", mode: 'copy', overwrite: true  

    input:
        path infile
        
    output:
        path "${infile}.line*"  //, emit: eingabedatei
        
    script:
        """
        split -l 2 ${infile} -d ${infile}.line  # how we name the files
        """
}

/*
process seq_lang {
  publishDir params.outdir, mode: 'copy', overwrite: true
  
  input:
    path infile
  output:
    path "${infile}.seq_lang", emit: GCcountent
  script:
    """
    python3 /home/alawady/meinrepos/testproject/seq_lang_ex3.py ${infile} > ${infile}.seq_lang
    """
}
*/


process calc_length {                          // process #2
    input :
        path seqfile
    output :
        path  "${seqfile}.laenge"                           // outfile
    script :
    """
    tail -n 1 ${seqfile} > sequenz 
    # cat sequenz | wc -m  > ${seqfile}.laenge 
    cat sequenz | tr -d "\n" | wc -m  > ${seqfile}.laenge 
    """
    
    //"""
    //tail -n 1 ${infile} > sequenz 
    //<sequenz wc -m> ${infile}.anzahl_nucleotids 
    //"""
    
    
    //"""
    //tail -n 1 ${infile} > sequenz
    //egrep -o -i 'A|C|G|T' sequenz > grepresult
    //cat grepresult | wc -l >> ${infile}.allcount
    //""" 
    
}

//def testffunkt (var1, var2):
//    return var1*var2
    
//test


process summary {
    publishDir "${params.outdir}", mode: 'copy', overwrite: true  

    input :
        path lenfiles
        
        
    output :
    
        path  "lenout"    , emit =  len   //   out files
        // path  "sumout"
    script :
    """
    # echo ${lenfiles} > lenout
    
    # echo ${lenfiles[0]} >> lenout
    
    cat ${lenfiles} > lenout
    
    # cat lenout | paste -sd "+" > sumout
    # cat lenout | wc -l 
    # cat lenout | paste -sd "+" > calc 
    # cat lenout | wc -l >> calc
    # cat calc | paste -sd "/" | bc 
    """
    
    
}





//////////////////////////////////////////////////////////////////////////////

workflow {
                                           // einlesen                    # 1
  // inchannel = channel.fromPath ("sequences.fasta")
  inchannel = channel.fromPath (params.infile)
  
  
                                            // spliten file              # 2
  //splitfiles = read_fasta(inchannel)
  
  splitfiles = split_file (inchannel)
  
                                            // aufgabe = länge bestimmen  # 3
  
  // Sequences_Lang = seq_lang(splitfiles.flatten())
  
  laengen = calc_length (splitfiles.flatten())
  
                                            //  zusammenfassung ergbniss  # 4
  
  ergbniss = summary(laengen.collect())
  
  
}



/*


process calc_length {                          // process #2

    input :
        path seqfile
        
        
    output :
    
        path 
    
    script :
    """
    
    """

    
}


*/



